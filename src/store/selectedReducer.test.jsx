import selectedReducer, {
    updateSelectedRemoveAC,
    toggleM,
    updateSelectedAddAC,
    updateSelectedSubAC
} from "./selectedReducer";
import favoritesReducer, {toggleF, updateFavoritesAddAC, updateFavoritesSubAC} from "./favoritesReducer";
import {updateCarAC} from "./carReducer";

describe('selectedReducer', () => {
    const initialState = {
        counterSelected: 0,
    };

    const mockDispatch = jest.fn();

    it('should return the initial state', () => {
        expect(selectedReducer(undefined, {})).toEqual(initialState);
    });

    it('should handle UPDATE_SELECTED_REMOVE', () => {
        const action = updateSelectedRemoveAC(5);
        expect(selectedReducer(initialState, action)).toEqual({
            counterSelected: 5,
        });
    });

    it('should handle UPDATE_SELECTED_ADD', () => {
        const action = updateSelectedAddAC();
        expect(selectedReducer(initialState, action)).toEqual({
            counterSelected: 1,
        });
    });

    it('should handle UPDATE_SELECTED_SUB', () => {
        const action = updateSelectedSubAC();
        expect(selectedReducer(initialState, action)).toEqual({
            counterSelected: -1,
        });
    });

    it('should handle the toggleM function and increment the Selected', () => {
        const sampleCar = {
            id: 1,
            buyCar: false,
        };
        const state = { cars: [sampleCar] };

        toggleM(0, state.cars, mockDispatch);

        expect(state.cars[0].buyCar).toBe(sampleCar.buyCar);
        expect(mockDispatch).toHaveBeenCalledWith(updateCarAC(expect.any(Array)));
        expect(mockDispatch).toHaveBeenCalledWith(updateSelectedAddAC());
    });

    it('should handle the toggleF function and decrement the counter', () => {
        const sampleCar = {
            id: 1,
            buyCar: true,
        };

        const state = { cars: [sampleCar] };

        toggleM(0, state.cars, mockDispatch);

        expect(state.cars[0].buyCar).toBe(sampleCar.buyCar);
        expect(mockDispatch).toHaveBeenCalledWith(updateCarAC(expect.any(Array)));
        expect(mockDispatch).toHaveBeenCalledWith(updateSelectedSubAC());
    });
});
