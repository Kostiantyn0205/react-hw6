import * as Yup from 'yup';

const validation = () => {
    return Yup.object().shape({
        firstName: Yup
            .string()
            .matches(/^[a-zA-Zа-яА-ЯёЁ]*$/, 'Username must not contain numbers')
            .required('Username is a required field'),
        lastName: Yup
            .string()
            .matches(/^[a-zA-Zа-яА-ЯёЁ]*$/, 'Username must not contain numbers')
            .required('Username is a required field'),
        age: Yup
            .number()
            .positive('User age must be a positive number')
            .min(18, 'The user must be over 18 years old')
            .required('User age is required'),
        address: Yup
            .string()
            .required('The delivery address is mandatory'),
        mobilePhone: Yup
            .string()
            .matches(/^\(\d{3}\) \d{3}-\d{2}-\d{2}$/, 'Phone number must be in the format (###) ###-##-##')
            .required('Phone number is required'),
    });
}

export default validation;