import React from "react";
import renderer from "react-test-renderer";
import '@testing-library/jest-dom';
import Button from './Button';

test('Button snapshot', () => {
    const component = renderer.create(<Button backgroundColor="blue" textColor="white" text="Click me" onClick={() => {}}/>);

    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});