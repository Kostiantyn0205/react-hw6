import carReducer from "./carReducer";

describe('carReducer', () => {
    it('should return the initial state', () => {
        const initialState = {cars: [],};
        expect(carReducer(undefined, {})).toEqual(initialState);
    });

    it('should handle UPDATE_CAR action', () => {
        const initialState = {
            cars: [{ idCar: 1, name: 'Car 1' }],
        };

        const newCars = [{ idCar: 2, name: 'Car 2' }];
        const action = { type: 'UPDATE_CAR', payload: newCars };

        expect(carReducer(initialState, action)).toEqual({cars: newCars,});
    });
});
