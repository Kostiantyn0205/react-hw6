import React from 'react';
import renderer from "react-test-renderer";
import '@testing-library/jest-dom';
import Modal from './Modal';

test('Modal snapshot', () => {
    const component = renderer.create(
        <Modal header="Test Modal" closeButton={true} onClick={() => {}} text="This is a test modal." actions={<button>Confirm</button>}/>
    );

    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});
