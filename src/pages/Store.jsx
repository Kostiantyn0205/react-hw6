import ListCard from "../components/ListCard;";
import PropTypes from "prop-types";
import {useViewMode} from "../context/ViewModeContext";
import IconList from "../icon/IconList";
import IconTable from "../icon/IconTable";

function Store() {
    const { viewMode, toggleViewMode } = useViewMode();
    return (
        <>
            <div className="button-view-container">
                <button className={`view button-list ${viewMode === 'table' && 'completed-view'}`} onClick={()=>toggleViewMode('list')}>
                   <IconList />
                </button>
                <button className={`view button-table ${viewMode === 'list' && 'completed-view'}`} onClick={()=>toggleViewMode('table')}>
                    <IconTable />
                </button>
            </div>
            <ListCard pageName={"Store"}></ListCard>
        </>
    )
}

Store.propTypes = {
    racingCars: PropTypes.array,
    toggleCounterSelected: PropTypes.func,
    toggleCounterFavorites: PropTypes.func
};

export default Store;