import React from 'react';
import {render, fireEvent, queryByText, getByTestId} from '@testing-library/react';
import Modal from './Modal';

describe('Modal component', () => {
    it('renders with default props', () => {
        const { getByText } = render(<Modal actions={<button>Close</button>}/>);

        expect(getByText('Modal window')).toBeInTheDocument();
        expect(getByText('Are you sure you want to do this?')).toBeInTheDocument();
        expect(getByText('Close')).toBeInTheDocument();
    });

    it('renders with custom props', () => {
        const { getByText, queryByText } = render(<Modal header="Custom Header" closeButton={false} text="Custom text content"/>);

        expect(getByText('Custom Header')).toBeInTheDocument();
        expect(queryByText('Close')).toBeNull();
        expect(getByText('Custom text content')).toBeInTheDocument();
    });

    it('calls onClick when clicking outside the modal', () => {
        const onClickMock = jest.fn();
        const { getByTestId } = render(<Modal onClick={onClickMock} />);

        fireEvent.click(getByTestId('modal-overlay'));
        expect(onClickMock).toHaveBeenCalledTimes(1);
    });

    it('calls onClick when clicking the close button', () => {
        const onClickMock = jest.fn();
        const { getByTestId } = render(<Modal onClick={onClickMock} />);

        fireEvent.click(getByTestId('modal-close-button'));
        expect(onClickMock).toHaveBeenCalledTimes(1);
    });
});
