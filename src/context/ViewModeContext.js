import { createContext, useContext, useState } from 'react';

const ViewModeContext = createContext();

export const ViewModeProvider = ({ children }) => {
    const localStorageViewMode = JSON.parse(localStorage.getItem("viewMode"));

    const [viewMode, setViewMode] = useState(localStorageViewMode || 'list' );

    const toggleViewMode = (prevMode= 'table') => {
        setViewMode(prevMode === 'list' ? 'table' : 'list');
    };

    return (
        <ViewModeContext.Provider value={{ viewMode, toggleViewMode }}>
            {children}
        </ViewModeContext.Provider>
    );
};

export const useViewMode = () => {
    return useContext(ViewModeContext);
};
