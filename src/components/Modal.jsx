import {useEffect} from "react";
import PropTypes from 'prop-types';

const Modal = ({ header, closeButton, onClick, text, actions }) => {
    const clickOutside = (event) => {
        if (!event.target.closest(".modal-container")) {
            onClick();
        }
    };

    return (
        <div className="modal-overlay" data-testid="modal-overlay" onClick={clickOutside}>
            <div className="modal-container">
                {closeButton && (<button data-testid="modal-close-button" className="modal-button-close" onClick={onClick}></button>)}
                <h2>{header}</h2>
                <p>{text}</p>
                {actions}
            </div>
        </div>
    );
};

Modal.propTypes = {
    header: PropTypes.string,
    closeButton: PropTypes.bool,
    onClick: PropTypes.func,
    text: PropTypes.string,
    actions: PropTypes.element
};

Modal.defaultProps = {
    header: "Modal window",
    closeButton: true,
    text: "Are you sure you want to do this?",
};

export default Modal;