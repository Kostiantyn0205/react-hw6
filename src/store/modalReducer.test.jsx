import modalReducer, { toggle } from "./modalReducer";

describe('modalReducer', () => {
    const initialState = {
        showModal: false,
        modals: {},
    };

    it('should return the initial state', () => {
        expect(modalReducer(undefined, {})).toEqual(initialState);
    });

    it('should handle TOGGLE_MODAL', () => {
        const cardId = 'exampleCardId';
        const modalState = true;
        const action = toggle(cardId, modalState);

        expect(modalReducer(initialState, action)).toEqual({
            showModal: false,
            modals: {
                [cardId]: modalState,
            },
        });
    });
});
