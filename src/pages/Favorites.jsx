import ListCard from "../components/ListCard;";
import PropTypes from "prop-types";

function Favorites(){
    return (
        <ListCard pageName="Favorites"></ListCard>
    )
}

Favorites.propTypes = {
    toggleCounterSelected: PropTypes.func,
    toggleCounterFavorites: PropTypes.func
};

export default Favorites;