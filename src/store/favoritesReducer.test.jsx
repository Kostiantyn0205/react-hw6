import favoritesReducer, { toggleF, updateFavoritesAC, updateFavoritesAddAC, updateFavoritesSubAC } from "./favoritesReducer";
import {updateCarAC} from "./carReducer";

describe('favoritesReducer', () => {
    const initialState = {
        counterFavorites: 0,
    };

    const mockDispatch = jest.fn();

    it('should return the initial state', () => {
        expect(favoritesReducer(undefined, {})).toEqual(initialState);
    });

    it('should handle UPDATE_FAVORITES', () => {
        const payload = 5;
        const action = updateFavoritesAC(payload);
        expect(favoritesReducer(initialState, action)).toEqual({
            counterFavorites: payload,
        });
    });

    it('should handle UPDATE_FAVORITES_ADD', () => {
        const action = updateFavoritesAddAC();
        expect(favoritesReducer(initialState, action)).toEqual({
            counterFavorites: 1,
        });
    });

    it('should handle UPDATE_FAVORITES_SUB', () => {
        const action = updateFavoritesSubAC();
        expect(favoritesReducer(initialState, action)).toEqual({
            counterFavorites: -1,
        });
    });

    it('should handle the toggleF function and increment the counter', () => {
        const sampleCar = {
            id: 1,
            completed: false,
        };

        const state = { cars: [sampleCar] };

        toggleF(0, state.cars, mockDispatch);

        expect(state.cars[0].completed).toBe(sampleCar.completed);
        expect(mockDispatch).toHaveBeenCalledWith(updateCarAC(expect.any(Array)));
        expect(mockDispatch).toHaveBeenCalledWith(updateFavoritesAddAC());
    });

    it('should handle the toggleF function and decrement the counter', () => {
        const sampleCar = {
            id: 1,
            completed: true,
        };

        const state = { cars: [sampleCar] };

        toggleF(0, state.cars, mockDispatch);

        expect(state.cars[0].completed).toBe(sampleCar.completed);
        expect(mockDispatch).toHaveBeenCalledWith(updateCarAC(expect.any(Array)));
        expect(mockDispatch).toHaveBeenCalledWith(updateFavoritesSubAC());
    });
});
