import React from 'react';
import {render, fireEvent, getByTestId} from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

import Button from './Button';

describe('Button component', () => {
    it('renders with the correct text', () => {
        const text = 'Click me';
        const { getByTestId } = render(<Button text={text} />);
        expect(getByTestId("Button")).toHaveTextContent(text);
    });

    it('calls the onClick function when clicked', () => {
        const onClickMock = jest.fn();
        const { getByTestId } = render(<Button onClick={onClickMock} text="Click me" />);
        fireEvent.click(getByTestId("Button"));
        expect(onClickMock).toHaveBeenCalled();
    });
});