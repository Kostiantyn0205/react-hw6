import './scss/App.scss';
import {useEffect} from 'react';
import {Routes, Route} from "react-router-dom";
import Store from "./pages/Store"
import Basket from "./pages/Basket"
import Favorites from "./pages/Favorites"
import Header from "./components/Header";
import {useDispatch, useSelector} from "react-redux";
import {fetchUsersThunk} from "./store/carReducer";
import {useViewMode} from "./context/ViewModeContext";

function App() {
    const dispatch = useDispatch();
    const localStorageDataCars = JSON.parse(localStorage.getItem("racingCars"));

    const cars = useSelector((state) => state.car.cars);
    const counterFavorites = useSelector((state) => state.favorites.counterFavorites);
    const counterSelected = useSelector((state) => state.choose.counterSelected);
    const {viewMode} = useViewMode();

    useEffect(() => {
        if(!localStorageDataCars) {
            dispatch(fetchUsersThunk());
        }
    }, []);

    useEffect(() => {
        localStorage.setItem("counterFavorites", JSON.stringify(counterFavorites));
        localStorage.setItem("counterSelected", JSON.stringify(counterSelected));
        localStorage.setItem("racingCars", JSON.stringify(cars));
        localStorage.setItem("viewMode", JSON.stringify(viewMode));
    }, [counterSelected, counterFavorites, cars, viewMode]);

    return (
        <>
            <Header />
            <Routes>
                <Route index path="/" element={<Store />}/>
                <Route path="/favorites" element={<Favorites />}/>
                <Route path="/basket" element={<Basket />}/>
            </Routes>
        </>
    );
}

export default App;